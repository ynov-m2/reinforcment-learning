# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'
# %%
import logging
logging.basicConfig(level=logging.DEBUG)
import numpy as np
import time, pickle, os
import random
import gym_mastermind
import gym


# %%
env = gym.make('Mastermind-v0')
env.reset()
epsilon = 0.9
total_episodes = 30000
max_steps = 100
lr_rate = 0.81 # Alpha ??
gamma = 0.96


# %%
print(env.observation_space[0].n)
print(env.action_space[0].n)
def sarsa_choose_action(state):
    state_index = get_index_from_tuple(state, False)
    action = 0
    if np.random.uniform(0, 1) < epsilon:
        action = env.action_space.sample()
    else:
        action_index = np.argmax(Q[state_index, :])
        action = index_to_tuple(action_index)
    
    return action

def qlearning_learn(state, state2, reward, action):
    state_index = get_index_from_tuple(state, False)
    state2_index = get_index_from_tuple(state2, False)
    action_index = get_index_from_tuple(action, True)
    old_value = Q[state_index, action_index]
    learned_value = reward + gamma * np.max(Q[state2_index, :])
    Q[state_index, action_index] = (1 - lr_rate) * old_value +  lr_rate * learned_value

def sarsa_learn(state, state2, reward, action, action2):
    state_index = get_index_from_tuple(state, False)
    action_index = get_index_from_tuple(action, True)
    state2_action = get_index_from_tuple(state2, False)
    action2_index = get_index_from_tuple(action2, True)
    old_value = Q[state_index, action_index]
    learned_value = reward + gamma * Q[state2_action, action2_index]
    Q[state_index, action_index] = (1 - lr_rate) * old_value +  lr_rate * learned_value

def get_index_unique(tuple_to_index, base_range):
    index_shifter = tuple_to_index[0] * base_range ** 3 +                     tuple_to_index[1] * base_range ** 2 +                     tuple_to_index[2] * base_range ** 1 +                     tuple_to_index[3] * base_range ** 0
    return index_shifter

def get_index_from_tuple(tuple_to_index, isAction):
    index = 0
    if isAction:
        base_range = env.action_space[0].n
    else:
        base_range = env.observation_space[0].n
    return get_index_unique(tuple_to_index, base_range)

def index_to_tuple(index):
     first_tuple_value = index // 6**3
     actual = index % 6**3
     second_tuple_value = actual // 6**2
     actual = index % 6**2
     third_tuple_value = actual // 6**1
     actual = index % 6**1
     fourth_tuple_value = actual // 6**0
     actual = index % 6**0
     return (first_tuple_value, second_tuple_value, third_tuple_value, fourth_tuple_value)


# %%
Q = np.zeros((env.observation_space[0].n**4, env.action_space[0].n**4))
# Start
for episode in range(total_episodes):
    state = env.reset()
    t = 0
    action = sarsa_choose_action(state) 
    while t < max_steps:
        # env.render()
        state2, reward, done, info = env.step(action)
        action2 = sarsa_choose_action(state2)
        sarsa_learn(state, state2, reward, action, action2)
        state = state2
        action = action2
        t += 1
        if done:
            break

print(Q)


# %%
def evaluate():
  total_epochs, total_penalties = 0, 0

  for _ in range(total_episodes):
      state = env.reset()
      epochs, penalties, reward = 0, 0, 0
      
      done = False
      
      while not done:
          state_index = get_index_from_tuple(state, False)
          action_index = np.argmax(Q[state_index, :])
          print(action_index)
          action = index_to_tuple(action_index)
          print(action)
          state, reward, done, info = env.step(action)

          if reward < 0:
              penalties += 1

          epochs += 1

      total_penalties += penalties
      total_epochs += epochs

  print(f"Results after {total_episodes} episodes:")
  print(f"Average timesteps per episode: {total_epochs / total_episodes}")
  print(f"Average penalties per episode: {total_penalties / total_episodes}")


# %%
evaluate()


# %%



